// initialize Account Kit with CSRF protection
AccountKit_OnInteractive = function () {

    AccountKit.init(
        {
            appId: "2048558575399403",
            state: "07582b1f-dda3-4d6d-8ba9-c2d499f5a183",
            version: "v1.0",
            fbAppEventsEnabled: true
            // redirect: "{{REDIRECT_URL}}"
        }
    );
};

// login callback
function loginCallback(response) {
    if (response.status === "PARTIALLY_AUTHENTICATED") {
        var code = response.code;
        var csrf = response.state;

        console.log(csrf);
        console.log(code);

        // Send code to server to exchange for access token
        document.getElementById("code").value = code;
        document.getElementById("csrf").value = csrf;

        document.getElementById("login_success").submit();

    }
    else if (response.status === "NOT_AUTHENTICATED") {
        // handle authentication failure
    }
    else if (response.status === "BAD_PARAMS") {
        // handle bad parameters
    }
}

// phone form submission handler
function smsLogin() {
    var countryCode = document.getElementById("country_code").value;
    var phoneNumber = document.getElementById("phone_number").value;
    AccountKit.login(
        'PHONE',
        {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
        loginCallback
    );
}
