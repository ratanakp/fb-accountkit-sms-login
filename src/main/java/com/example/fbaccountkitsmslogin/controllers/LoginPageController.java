package com.example.fbaccountkitsmslogin.controllers;

import com.example.fbaccountkitsmslogin.models.AccountKitCodeCsrf;
import com.example.fbaccountkitsmslogin.models.Role;
import com.example.fbaccountkitsmslogin.models.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LoginPageController {

    private static final String FB_AK_APP_SECRET = "4099867570d4eb670c3ac5d78555ba63";
    private static final String FB_APP_ID = "2048558575399403";

    private static final String AK_GRAPH_ME = "https://graph.accountkit.com/v1.0/me/";
    private static final String EXCHANGE_USER_ACCESS_TOKEN = "https://graph.accountkit.com/v1.0/access_token?grant_type=authorization_code&code=";

    private RestTemplate restTemplate = new RestTemplate();


    @GetMapping("/login")
    public String loginPage(Model model) {
        model.addAttribute("akCodeCsrf", new AccountKitCodeCsrf());
        return "login";
    }


    @PostMapping("/redirect/login_success")
    public String success(@ModelAttribute("akCodeCsrf") AccountKitCodeCsrf accountKitCodeCsrf) {

        String url = EXCHANGE_USER_ACCESS_TOKEN + accountKitCodeCsrf.getCode() + "&access_token=AA|" + FB_APP_ID + "|" + FB_AK_APP_SECRET;

        System.out.println(url);

        Object object = restTemplate.getForObject(url, Object.class);

        System.out.println(object);

        System.out.println(accountKitCodeCsrf);

        Map<String, Object> userIdAccessToken = (Map<String, Object>) object;

        String id = (String) userIdAccessToken.get("id");

        /*
         *
         * TODO: Use id to check in database for available user or not, if not return to sign up page, if yes query that user and set auth as below
         *
         * */

        String access_token = (String) userIdAccessToken.get("access_token");
        System.out.println(access_token);

        String ak_graph_me_url = AK_GRAPH_ME + "?access_token=" + access_token;

        System.out.println(ak_graph_me_url);

        Object my_profile = restTemplate.getForObject(ak_graph_me_url, Object.class);

        System.out.println(my_profile);

        @SuppressWarnings("unchecked")
        Map<String, Object> myInfo = (HashMap<String, Object>) my_profile;

        System.out.println(myInfo.get("id"));

        Map<String, Object> phone = (Map<String, Object>) myInfo.get("phone");
        String phone_number = (String) phone.get("number");

        //Save some user info here
        List<Role> roles = new ArrayList<>();
        roles.add(new Role(1, "USER_SMS_LOGIN"));

        User user = new User();
        user.setUsername("Phang Ratanak");
        user.setFbId(id);
        user.setAccessToken(access_token);
        user.setPhone(phone_number);
        user.setRoles(roles);
        //End save some user info

        /*
         *
         * TODO: set auth
         *
         * */
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, user.getRoles());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return "redirect:/";

    }


    @GetMapping({"/", "/index"})
    public String index() {
        return "index";
    }
}
