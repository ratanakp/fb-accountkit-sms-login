package com.example.fbaccountkitsmslogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FbAccountkitSmsLoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(FbAccountkitSmsLoginApplication.class, args);
    }
}
