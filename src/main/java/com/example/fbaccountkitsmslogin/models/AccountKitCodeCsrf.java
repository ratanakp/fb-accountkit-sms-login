package com.example.fbaccountkitsmslogin.models;

public class AccountKitCodeCsrf {

    private String code;
    private String csrf;

    public AccountKitCodeCsrf() {
    }

    public AccountKitCodeCsrf(String code, String csrf) {
        this.code = code;
        this.csrf = csrf;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCsrf() {
        return csrf;
    }

    public void setCsrf(String csrf) {
        this.csrf = csrf;
    }

    @Override
    public String toString() {
        return "AccountKitCodeCsrf{" +
                "code='" + code + '\'' +
                ", csrf='" + csrf + '\'' +
                '}';
    }
}
