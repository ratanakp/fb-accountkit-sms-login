package com.example.fbaccountkitsmslogin.models;

import java.util.List;

public class User {

    private Integer id;
    private String fbId;
    private String username;
    private String phone;
    private String accessToken;
    private List<Role> roles;

    public User() {
    }

    public User(Integer id, String fbId, String username, String phone, String accessToken, List<Role> roles) {
        this.id = id;
        this.fbId = fbId;
        this.username = username;
        this.phone = phone;
        this.accessToken = accessToken;
        this.roles = roles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", fbId='" + fbId + '\'' +
                ", username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", roles=" + roles +
                '}';
    }
}
